package modele;

public class Client implements IClient {

	private int idClient;
	private String nomClient;
	private String prenomClient;
	private String adresseClient;
	private String villeClient;
	private ICompte compteClient;
	

	public Client(int idClient, String nomClient, String prenomClient,
			String adresseClient, String villeClient) {
		super();
		this.idClient = idClient;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.adresseClient = adresseClient;
		this.villeClient = villeClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public void setAdresseClient(String adresseClient) {
		this.adresseClient = adresseClient;
	}

	public void setVilleClient(String villeClient) {
		this.villeClient = villeClient;
	}

	public int getIdClient() {
		// TODO Auto-generated method stub
		return this.idClient;
	}

	public String getNomClient() {
		// TODO Auto-generated method stub
		return this.nomClient;
	}

	public String getPrenomClient() {
		// TODO Auto-generated method stub
		return this.prenomClient;
	}

	public String getAdresseClient() {
		// TODO Auto-generated method stub
		return this.adresseClient;
	}

	public String getVilleClient() {
		// TODO Auto-generated method stub
		return this.villeClient;
	}

	public ICompte getCompteClient() {
		// TODO Auto-generated method stub
		return this.compteClient;
	}

	public void setCompteClient(ICompte c) {
		this.compteClient =c ;

	}

	public String toString(){
		return nomClient+"/"+prenomClient+"/"+adresseClient+"/"+villeClient+"/"+compteClient;
	}
}
