package modele;

public interface IClient {

	public int getIdClient();
	public String getNomClient();
	public String getPrenomClient();
	public String getAdresseClient();
	public String getVilleClient();
	public ICompte getCompteClient();
	public void setCompteClient(ICompte c);
}
