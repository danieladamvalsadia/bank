package modele;

public class Compte implements ICompte {

	private double solde;
	private int idCompte;
	
	
	public Compte(){
		this.solde = 0;
		this.idCompte = -1;
	}
	public Compte(double solde,int solde2){
		this.solde = solde;
		this.idCompte=solde2;
	}
	public void crediterCompte(double montant) {
		this.solde+=montant;

	}

	public void debiterCompte(double montant) {
		this.solde -= montant;
	}

	public double getSolde() {
		// TODO Auto-generated method stub
		return this.solde;
	}
	public int getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}
	
	public String toString(){
		return solde+"";
	}

}
